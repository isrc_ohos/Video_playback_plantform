# 鸿蒙视频播放平台

本应用是基于多个鸿蒙开源第三方组件快速开发的一款基础功能齐全的视频播放平台。

## Video_playback_plantform介绍

* 项目名称：[Video_playback_plantform](https://gitee.com/isrc_ohos/Video_playback_plantform)

* 项目功能：视频播放、视频浏览、视频缓存、裁剪头像、轮播图片、滑动验证、扫码识别、流量统计等

* 所属系列：鸿蒙应用开发

* 开发版本：DevEco Studio 3.0 Beta1

* 项目作者和维护人：戴研

* 邮箱：daiyan@iscas.ac.cn


## 效果展示

应用展示：

![image-20211220154526347](C:/Users/zhangxinxin/AppData/Roaming/Typora/typora-user-images/image-20211220154526347.png)

![image-20211220154551468](C:\Users\zhangxinxin\AppData\Roaming\Typora\typora-user-images\image-20211220154551468.png)

![image-20211220154644803](C:\Users\zhangxinxin\AppData\Roaming\Typora\typora-user-images\image-20211220154644803.png)

![image-20211220153534879](C:\Users\zhangxinxin\AppData\Roaming\Typora\typora-user-images\image-20211220153534879.png)

![image-20211220153551168](C:\Users\zhangxinxin\AppData\Roaming\Typora\typora-user-images\image-20211220153551168.png)



## 视频播放平台工程结构

本应用共有5个主要的展示界面，具体页面如下介绍。

* MainSlice:   项目主界面

* MineSlice：“我的”界面 

* LoginSlice：  用户登录界面

* PlayingSlice: 视频播放界面

* ZBarSlice:    扫码界面

  ![image-20211220154427498](C:\Users\zhangxinxin\AppData\Roaming\Typora\typora-user-images\image-20211220154427498.png)

## 视频播放平台应用说明

Video_playback_plantform应用使用方法介绍如下：

1.git clone本应用组件到本地。

2.使用DevEco Studio 3.0 Beta1打开项目，项目可直接运行。

3.点击APP图标，启动项目，进入登录界面，该界面插入滑动验证组件。

```java
//滑动验证
DirectionalLayout directionalLayout=(DirectionalLayout) findComponentById(ResourceTable.Id_puzzle);
directionalLayout.addComponent(new puzzleLayout(this));
//登录按键
Button login_button=(Button) findComponentById(ResourceTable.Id_login_button);
```

4.主界面

​	主界面内容主要包括图片轮播区和视频推荐区。

1）图片轮播区点击进入轮播图片详情页，该页采用Banner轮播组件。

```java
//调用banner组件
//创建实体对象banner
Banner banner = (Banner) findComponentById(ResourceTable.Id_banner);
//设置监听器
banner.setOnBannerListener(this);
//设置banner对象的图片list（广告图片）和介绍title（广告文字介绍）以及其他属性信息
banner.setImages(list).setBannerTitles(title).setScaleType(3).setDelayTime(3000).setBannerStyle(5).setTitleTextSize(60).start();
```

2）视频推荐区使用ScrollView组件来实现页面的上下滑动，ScrollView是一种带滚动功能的组件，通过点击事件触发监听器可以进入视频播放界面。

```
//scrollview播放视频
findComponentById(ResourceTable.Id_scrollview_playing).setClickedListener(new Component.ClickedListener() {
    @Override
    public void onClick(Component Component) {
      // present(new PlayingSlice(),new Intent());
      Intent secondIntent = new Intent();
      // 指定待启动FA的bundleName和abilityName
      Operation operation = new Intent.OperationBuilder()
          .withDeviceId("")
          .withBundleName("com.huawei.mytestproject")
          .withAbilityName("com.huawei.mytestproject.PlayingAbility")
          .build();
      secondIntent.setOperation(operation);
      startAbility(secondIntent);
    }
  });
}
```

5.视频播放界面

​	视频播放功能的实现主要包括视频显示、视频播放器、视频缓存、进度控制、聊天室五大部分。

1）视频显示主要包括SurfaceProvider、AVLoadingIndicatorView_ohos，其中SurfaceProvider用于视频显示、AVLoadingIndicatorView_ohos用于动画加载。

```java
SurfaceProvider surfaceProvider;
surfaceProvider = (SurfaceProvider)findComponentById(ResourceTable.Id_surfaceprovider);
surfaceProvider.setVisibility(Component.HIDE);
surfaceProvider.getSurfaceOps().get().addCallback(this);
surfaceProvider.pinToZTop(true); 
```

2）视频播放器：Player。

```
Player player=new Player(this);
```

3）视频缓存：VideoCache_ohos组件。

```
public static HttpProxyCacheServer mCacheServerProxy = null;
if (mCacheServerProxy == null) mCacheServerProxy = new HttpProxyCacheServer(this);
//设置监听器
mCacheServerProxy.registerCacheListener(new CacheListener() {
  @Override
  public void onCacheAvailable(File cacheFile, String url, int percentsAvailable) {
    percents =percentsAvailable;
    if (percentsAvailable == 99) {
      mCacheServerProxy.shutdown();
      mCacheServerProxy.unregisterCacheListener(this);
    }
  }
}, URL);
```

4）进度控制部分主要运用Slider、Button控制进度条和按钮。

```
slider.setValueChangedListener(new Slider.ValueChangedListener() {
  @Overrided
  public void onProgressUpdated(Slider slider, int i, boolean b) {
    if (b && i < percents) player.rewindTo(player.getDuration() * (i-1) * 10);
  }
  @Override
  public void onTouchStart(Slider slider) {
  }
  @Override
  public void onTouchEnd(Slider slider) {
  }
});
```

5）聊天室部分主要运用ScrollView、ListContainer、BaseItemProvider组件来实现聊天功能。

```
//聊天框
        ScrollView s=(ScrollView)findComponentById(ResourceTable.Id_scrollview);
        
//设置显示从x到y行数据
    private void setlist(int x,int y){
        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_container);
        ArrayList<SettingItem> data = new ArrayList<>();
        for (int i = x; i <= y; i++) {
            data.add(new SettingItem(
                    "Name " + i+":",
                    "Massage  "+i
            ));
        }
        listContainer.setItemProvider(new SettingProvider(data, this));
        listContainer.invalidate();
    }
    
class SettingProvider extends BaseItemProvider{
    private List<SettingItem> settingList;
    private AbilitySlice slice;
    public SettingProvider(List<SettingItem> list, AbilitySlice slice) {
        this.settingList = list;
        this.slice = slice;
    }
```

6.“我的”界面

​	"我的"界面主要向用户展示头像和个人信息，其中头像具有头像裁剪功能，头像裁剪运用的是urop组件。

```java
UCropView uCropView = (UCropView) findComponentById(ResourceTable.Id_uCropView);        try {            uCropView.getCropImageView().setImageUri(uri_i, uri_o);            uCropView.getOverlayView().setShowCropFrame(true);            uCropView.getOverlayView().setShowCropGrid(true);            uCropView.getOverlayView().setDimmedColor(Color.TRANSPARENT.getValue());        } catch (Exception e) {            e.printStackTrace();        }
```

7.扫码界面

​	用户可通过扫描条形码或者二维码的形式获取条码信息。

```java
//初始化UI和相机，实现视频帧的获取        SurfaceProvider surfaceProvider = (SurfaceProvider) findComponentById(ResourceTable.Id_zbar_surfaceprovider);        surfaceProvider.getSurfaceOps().get().addCallback(new SurfaceOps.Callback() {            @Override            public void surfaceCreated(SurfaceOps surfaceOps) {                previewSurface = surfaceOps.getSurface();                openCamera();            }            @Override            public void surfaceChanged(SurfaceOps surfaceOps, int i, int i1, int i2) {            }            @Override            public void surfaceDestroyed(SurfaceOps surfaceOps) {            }
```

8.流量统计界面

​	平台流量统计功能的实现主要使用的是MPAndroidChart_ohos组件,可用于统计平台的用户访问量、各类作品的数量、用户自己发布视频的访问量等信息。

```
LimitLine ll1 = new LimitLine(52f, "实时流量统计");        ll1.setLabelPosition(LimitLabelPosition.LEFT_TOP);        ll1.setTextSize(14f);        ll1.setTextColor(Color.WHITE.getValue());        chart.getAxisLeft().addLimitLine(ll1);
```

## 版权和许可信息

Video_playback_plantform经过Apache License, version 2.0授权许可。






