package com.huawei.mytestproject;

import com.huawei.mytestproject.slice.loginSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class LoginAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(loginSlice.class.getName());
        requestPermissionsFromUser(
                new String[]{

                        "ohos.permission.CAMERA",
                        "ohos.permission.READ_MEDIA"
                }, 0);

        String[] permission = {"ohos.permission.INTERNET", "ohos.permission.WRITE_EXTERNAL_STORAGE",
                "ohos.permission.READ_EXTERNAL_STORAGE","ohos.permission.READ_MEDIA","ohos.permission.WRITE_MEDIA",  "ohos.permission.READ_USER_STORAGE", "ohos.permission.WRITE_USER_STORAGE"};
        for (int i=0;i<permission.length;i++){
            if(verifyCallingOrSelfPermission(permission[i]) != 0){
                if(canRequestPermission(permission[i])){
                    requestPermissionsFromUser(permission, 0);
                }
            }
        }
    }
}
