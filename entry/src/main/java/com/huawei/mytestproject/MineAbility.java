package com.huawei.mytestproject;

import com.huawei.mytestproject.slice.MineSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MineAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MineSlice.class.getName());
    }
}
