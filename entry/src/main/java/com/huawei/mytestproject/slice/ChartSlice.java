package com.huawei.mytestproject.slice;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.LimitLine.LimitLabelPosition;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import com.huawei.mytestproject.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Color;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import java.util.ArrayList;

public class ChartSlice extends AbilitySlice {

    private LineChart chart;

    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_chart);

        chart = new LineChart(this);
        chart.getAxisRight().setEnabled(false);
        chart.getAxisLeft().setAxisMaximum(55f);
        chart.getAxisLeft().setAxisMinimum(-11f);

        LimitLine ll1 = new LimitLine(52f, "实时流量统计");
        ll1.setLabelPosition(LimitLabelPosition.LEFT_TOP);
        ll1.setTextSize(14f);
        ll1.setTextColor(Color.WHITE.getValue());
        chart.getAxisLeft().addLimitLine(ll1);

        LineData data = setData(20, 50);

        findComponentById(ResourceTable.Id_chart_component).addDrawTask(new Component.DrawTask() {
            @Override
            public void onDraw(Component component, Canvas canvas) {
                chart.onDraw(canvas, data);
            }
        });
        findComponentById(ResourceTable.Id_chart_component).invalidate();

        //刷新按钮
        findComponentById(ResourceTable.Id_tab_fr).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
            present(new ChartSlice(),new Intent());
            setData(20, 50);
            }
        });
//
//        EventHandler handler = new EventHandler(EventRunner.create(true));
//        handler.postTask(new Runnable() {
//            @Override
//            public void run() {
//                getUITaskDispatcher().asyncDispatch(new Runnable(){
//                    @Override
//                    public void run() {
//            present(new ChartSlice(),new Intent());
//            setData(20, 50);
//                    }
//                });
//                //handler.postTask(this, 5000);
//            }
//        }, 2000);

        //返回按钮
        findComponentById(ResourceTable.Id_tab_back).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent secondIntent = new Intent();
                secondIntent.setOperation(new Intent.OperationBuilder()
                        .withBundleName("com.huawei.mytestproject")
                        .withAbilityName("com.huawei.mytestproject.MainAbility")
                        .build());
                startAbility(secondIntent);
            }
        });
    }
    private LineData setData(int count, float range) {
        ArrayList<Entry> values = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            float val = (float) (Math.random() * range);
            values.add(new Entry(i, val));
        }
        LineDataSet set1 = new LineDataSet(values, "实时用户访问量");
        set1.setColor(Color.YELLOW.getValue());
        set1.setCircleColor(Color.BLACK.getValue());
        set1.setValueTextSize(9f);
        set1.setDrawFilled(true);
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);
        LineData data = new LineData(dataSets);
        chart.setData(data);
        return data;
        }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
