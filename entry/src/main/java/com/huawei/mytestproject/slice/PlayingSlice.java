package com.huawei.mytestproject.slice;

import com.danikula.videocache.CacheListener;
import com.danikula.videocache.HttpProxyCacheServer;
import com.huawei.mytestproject.ResourceTable;
import com.wang.avi.indicators.BallSpinFadeLoaderIndicator;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.components.surfaceprovider.SurfaceProvider;
import ohos.agp.graphics.SurfaceOps;
import ohos.agp.utils.Color;
import ohos.app.dispatcher.task.Revocable;
import ohos.app.dispatcher.task.TaskPriority;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.common.Source;
import ohos.media.player.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PlayingSlice extends AbilitySlice implements SurfaceOps.Callback {
//    public static final String URL ="https://1251316161.vod2.myqcloud.com/5f6ddb64vodsh1251316161/6950eead5285890812637029535/iUn2HnRCGxMA.mp4";
    //public static final St ring URL ="https://www.bilibili.com/video/av1415480/";
   // public static final String URL ="https://www.bilibili.com/video/BV17g411g76Z?share_source=copy_web";
    public static final String URL ="https://zhuwei449.github.io/video4.mp4";
//    public static final String URL ="https://d1124.github.io/1.mp4";
//    public static final String URL ="https://gitee.com/D1124/test2/raw/master/1.mp4";
    private Player player=new Player(this);

    public static HttpProxyCacheServer mCacheServerProxy = null;
    SurfaceProvider surfaceProvider;
    private Slider slider;
    public static int percents = 0,x=1,p=0,time=100;
    public EventHandler handler = new EventHandler(EventRunner.create(true));
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        //设置xml
        setUIContent(ResourceTable.Layout_playing);
        //设置聊天框内容
        setlist(x,x+=4);
        //设置surfaceProvider
        surfaceProvider =(SurfaceProvider) findComponentById(ResourceTable.Id_surfaceprovider);
        surfaceProvider.setVisibility(Component.HIDE);
        surfaceProvider.getSurfaceOps().get().addCallback(this);
        surfaceProvider.pinToZTop(true);

        //设置滚动条
        slider = (Slider) findComponentById(ResourceTable.Id_slider);
        slider.setDividerLineColor(new Color(Color.rgb(13, 245, 123)));
        slider.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                if (b && i < percents) player.rewindTo(player.getDuration() * (i-1) * 10);
            }

            @Override
            public void onTouchStart(Slider slider) {
            }

            @Override
            public void onTouchEnd(Slider slider) {
            }
        });

        //开始时判断如果缓存文件存在则删除
//        if(p==0) {
//            String cacheDirPath = "/data/user/0/" + this.getBundleName() + "/cache/";
//            File appCacheDir = new File(cacheDirPath, "video-cache/24ee19fee1cfb4fd6dc1092709dc5813.mp4.download");
//            File appCacheDir2 = new File(cacheDirPath, "video-cache/24ee19fee1cfb4fd6dc1092709dc5813.mp4");
//            if (appCacheDir != null&&appCacheDir != null) {appCacheDir.delete();appCacheDir2.delete();}
//            p=1;
//        }
        //Cache服务
        if (mCacheServerProxy == null) mCacheServerProxy = new HttpProxyCacheServer(this);
        Revocable revocable =getGlobalTaskDispatcher(TaskPriority.DEFAULT).asyncDispatch(new Runnable() {
            @Override
            public void run() {
                //设置监听器
                mCacheServerProxy.registerCacheListener(new CacheListener() {
                    @Override
                    public void onCacheAvailable(File cacheFile, String url, int percentsAvailable) {
                        percents =percentsAvailable;
                        if(p==0) {
                            File appCacheDir = new File(cacheFile.getPath());
                            if (appCacheDir != null&&appCacheDir != null) {appCacheDir.delete();}
                            p=1;
                        }

                        HiLog.info(new HiLogLabel(3,0,"cache"),"cachefile:"+cacheFile.getPath().toString());
                        if (percentsAvailable == 99) {
                            mCacheServerProxy.shutdown();
                            mCacheServerProxy.unregisterCacheListener(this);

                        }
                    }
                }, URL);
                player.setSource(new Source(mCacheServerProxy.getProxyUrl(URL)));
                player.prepare();
                player.play();

            }
            });
        //聊天框
        ScrollView s=(ScrollView)findComponentById(ResourceTable.Id_scrollview);
        //聊天框刷新的加载动画
        DirectionalLayout layout=(DirectionalLayout)findComponentById(ResourceTable.Id_directionlayout2);
        //加载动画
        BallSpinFadeLoaderIndicator ballSpinFadeLoaderIndicator=new BallSpinFadeLoaderIndicator(this);
        DirectionalLayout.LayoutConfig ballSpinFadeLoaderIndicatorConfig = new DirectionalLayout.LayoutConfig(200, 200);
        ballSpinFadeLoaderIndicatorConfig.setMargins(430,220,0,0);
        ballSpinFadeLoaderIndicator.setLayoutConfig(ballSpinFadeLoaderIndicatorConfig);
        ballSpinFadeLoaderIndicator.setVisibility(Component.HIDE);
        //将加载动画BallSpinFadeLoaderIndicator添加进directionalLayout2
        layout.addComponent(ballSpinFadeLoaderIndicator);
        //动画开始
        ballSpinFadeLoaderIndicator.start();
        //进入界面加载视频的加载动画
        BallSpinFadeLoaderIndicator ballSpinFadeLoaderIndicator1=new BallSpinFadeLoaderIndicator(this);
        ballSpinFadeLoaderIndicator1.setLayoutConfig(ballSpinFadeLoaderIndicatorConfig);
        DirectionalLayout directionalLayout=(DirectionalLayout) findComponentById(ResourceTable.Id_directionlayout_playing);
        directionalLayout.addComponent(ballSpinFadeLoaderIndicator1);
        ballSpinFadeLoaderIndicator1.start();
        //绑定进度条
        Runnable run=new Runnable() {
            @Override
            public void run() {
                getUITaskDispatcher().asyncDispatch(new Runnable(){
                    @Override
                    public void run() {
                        try {
                            slider.setViceProgress(percents);
                            if(percents>0&&player.isNowPlaying()){
                                directionalLayout.setVisibility(Component.INVISIBLE);
                                surfaceProvider.setVisibility(Component.VISIBLE);
                                time=player.getDuration()/100;
                                HiLog.info(new HiLogLabel(3, 0, "cache"), "iyyyyyyyyyy:" + player.getCurrentTime() / time);
                                slider.setProgressValue(player.getCurrentTime() / time);
                            }
                            HiLog.info(new HiLogLabel(3,0,"cache"),"ggggggg:"+ percents);

                        } finally {}
                    }
                });
                handler.postTask(this, 1000);
            }
        };
        handler.postTask(run, 2000);


        //播放按键
        Image play=(Image)findComponentById(ResourceTable.Id_play);
        Image pause=(Image)findComponentById(ResourceTable.Id_pause);
        findComponentById(ResourceTable.Id_play).setClickedListener(Component -> {
            player.play();
            play.setVisibility(ohos.agp.components.Component.HIDE);
            pause.setVisibility(ohos.agp.components.Component.VISIBLE);
        });
        //暂停按键
        findComponentById(ResourceTable.Id_pause).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component Component) {
                player.pause();
                play.setVisibility(ohos.agp.components.Component.VISIBLE);
                pause.setVisibility(ohos.agp.components.Component.HIDE);
            }
        });
        //返回按键
        findComponentById(ResourceTable.Id_tab_back).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                slider.invalidate();
                handler.removeTask(run);
                revocable.revoke();
                player.stop();

                Intent secondIntent = new Intent();
                secondIntent.setOperation(new Intent.OperationBuilder()
                        .withBundleName("com.huawei.mytestproject")
                        .withAbilityName("com.huawei.mytestproject.MainAbility")
                        .build());
                startAbility(secondIntent);
            }
        });
        //刷新按键
        findComponentById(ResourceTable.Id_refresh).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component Component) {
                s.setVisibility(ohos.agp.components.Component.HIDE);
                ballSpinFadeLoaderIndicator.setVisibility(ohos.agp.components.Component.VISIBLE);
                Runnable ball=new Runnable() {
                    @Override
                    public void run() {
                        getUITaskDispatcher().asyncDispatch(new Runnable(){

                            @Override
                            public void run() {
                                s.setVisibility(ohos.agp.components.Component.VISIBLE);
                                ballSpinFadeLoaderIndicator.setVisibility(ohos.agp.components.Component.INVISIBLE);
                            }
                        });

                    }
                };
                handler.postTask(ball,1000);
                s.fluentScrollByY(-400);
                //每次添加5行数据
                setlist(x,x+=4);

            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }
    @Override
    public void surfaceCreated(SurfaceOps surfaceOps) {
        player.setSurfaceOps(surfaceProvider.getSurfaceOps().get());
        player.prepare();
    }
    @Override
    public void surfaceChanged(SurfaceOps surfaceOps, int i, int i1, int i2) {}
    @Override
    public void surfaceDestroyed(SurfaceOps surfaceOps) {
    }

    @Override
    public void onStop(){
    }
    //设置显示从x到y行数据
    private void setlist(int x,int y){
        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_container);
        ArrayList<SettingItem> data = new ArrayList<>();
        for (int i = x; i <= y; i++) {
            data.add(new SettingItem(
                    "Name " + i+":",
                    "Massage  "+i
            ));
        }
        listContainer.setItemProvider(new SettingProvider(data, this));
        listContainer.invalidate();
    }

//实体类
class SettingItem {
    private String settingName;
    private String settingMassage;
    public SettingItem(String settingName,String settingMassage) {
        this.settingName = settingName;
        this.settingMassage = settingMassage;
    }
    public String getSettingName() {
        return settingName;
    }
    public String getSettingMassage() {
        return settingMassage;
    }
}

class SettingProvider extends BaseItemProvider{
    private List<SettingItem> settingList;
    private AbilitySlice slice;
    public SettingProvider(List<SettingItem> list, AbilitySlice slice) {
        this.settingList = list;
        this.slice = slice;
    }
    @Override
    public int getCount() {
        return settingList == null ? 0 : settingList.size();
    }
    @Override
    public Object getItem(int position) {
        if (settingList != null && position >= 0 && position < settingList.size()){
            return settingList.get(position);
        }
        return null;
    }
    @Override
    public long getItemId(int i) {
        return i;
    }
    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        final Component cpt=LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_item, null, false);
        SettingItem setting = settingList.get(i);
        Text settingText=(Text) cpt.findComponentById(ResourceTable.Id_text_setting);
        Text settingText2 = (Text) cpt.findComponentById(ResourceTable.Id_text_setting2);
        settingText.setText(setting.getSettingName());
        settingText2.setText(setting.getSettingMassage());
        return cpt;
    }
}
}
