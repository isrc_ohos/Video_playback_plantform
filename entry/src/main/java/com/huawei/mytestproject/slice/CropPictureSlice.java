package com.huawei.mytestproject.slice;

import com.huawei.mytestproject.ResourceTable;
import com.yalantis.ucrop.view.GestureCropImageView;
import com.yalantis.ucrop.view.UCropView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.data.rdb.ValuesBucket;
import ohos.global.icu.text.DecimalFormat;
import ohos.media.image.ImagePacker;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class CropPictureSlice extends AbilitySlice {

    private GestureCropImageView mGestureCropImageView;
    //缩放和旋转参数
    private DecimalFormat df = new DecimalFormat("#.00");

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_crop);
        //网图URI
//        uri_i = Uri.parse("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fa0.att.hudong.com%2F30%2F29%2F01300000201438121627296084016.jpg&refer=http%3A%2F%2Fa0.att.hudong.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1617882104&t=a1fdb27b4652aee1f5f48fc8d024a1bb");
        Uri uri_i = intent.getUri();
        //本地URI
        String filename = "test.jpg";
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size = new Size(100,100);
        PixelMap pixelmap = PixelMap.create(options);
        Uri uri_o = saveImage(filename, pixelmap);
        UCropView uCropView = (UCropView) findComponentById(ResourceTable.Id_uCropView);
        try {
            uCropView.getCropImageView().setImageUri(uri_i, uri_o);
            uCropView.getOverlayView().setShowCropFrame(true);
            uCropView.getOverlayView().setShowCropGrid(true);
            uCropView.getOverlayView().setDimmedColor(Color.TRANSPARENT.getValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
        findComponentById(ResourceTable.Id_crop).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mGestureCropImageView = uCropView.getCropImageView();
                mGestureCropImageView.cropAndSaveImage("image/jpg", 90, null);
                setFlag(true);
                setvi(true);
                present(new MineSlice(),new Intent());

            }
        });
        {
            //旋转部分
            // Text
            Text text = (Text) findComponentById(ResourceTable.Id_text_rotation);
            text.setText("当前旋转角度: 0 °");
            //旋转+90Button
            findComponentById(ResourceTable.Id_plus).setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    uCropView.getCropImageView().postRotate(90f, uCropView.getOverlayView().getCropViewRect().getCenter().getPointX(), uCropView.getOverlayView().getCropViewRect().getCenter().getPointY());
                    uCropView.getCropImageView().setImageToWrapCropBounds(false);
                    text.setText("当前旋转角度: " + df.format(uCropView.getCropImageView().getCurrentAngle()) + " °");
                }
            });
            findComponentById(ResourceTable.Id_minus).setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    uCropView.getCropImageView().postRotate(-90f, uCropView.getOverlayView().getCropViewRect().getCenter().getPointX(), uCropView.getOverlayView().getCropViewRect().getCenter().getPointY());
                    uCropView.getCropImageView().setImageToWrapCropBounds(false);
                    text.setText("当前旋转角度: " + df.format(uCropView.getCropImageView().getCurrentAngle()) + " °");
                }
            });
            //旋转Slider
            Slider slider_rotation = (Slider) findComponentById(ResourceTable.Id_rotation);
            slider_rotation.setValueChangedListener(new Slider.ValueChangedListener() {
                @Override
                public void onProgressUpdated(Slider slider, int i, boolean b) {
                    uCropView.getCropImageView().postRotate(0.05f * (i-50f), uCropView.getOverlayView().getCropViewRect().getCenter().getPointX(), uCropView.getOverlayView().getCropViewRect().getCenter().getPointY());
                    text.setText("当前旋转角度: " + df.format(uCropView.getCropImageView().getCurrentAngle()) + " °");
                }

                @Override
                public void onTouchStart(Slider slider) {

                }

                @Override
                public void onTouchEnd(Slider slider) {
                    uCropView.getCropImageView().setImageToWrapCropBounds(false);
                    slider.setProgressValue(50);
                    text.setText("当前旋转角度: " + df.format(uCropView.getCropImageView().getCurrentAngle()) + " °");
                }
            });

        }
        {
            //缩放Text
            Text text2 = (Text) findComponentById(ResourceTable.Id_text_scale);
            text2.setText("当前缩放倍数: x1");
            //缩放x2Button
            findComponentById(ResourceTable.Id_plus2).setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    uCropView.getCropImageView().postScale(2f, uCropView.getOverlayView().getCropViewRect().getCenter().getPointX(), uCropView.getOverlayView().getCropViewRect().getCenter().getPointY());
                    uCropView.getCropImageView().setImageToWrapCropBounds(false);
                    text2.setText("当前缩放倍数: x" + df.format(uCropView.getCropImageView().getCurrentScale()));
                }
            });
            //缩放/2Button
            findComponentById(ResourceTable.Id_minus2).setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    uCropView.getCropImageView().postScale(0.5f, uCropView.getOverlayView().getCropViewRect().getCenter().getPointX(), uCropView.getOverlayView().getCropViewRect().getCenter().getPointY());
                    uCropView.getCropImageView().setImageToWrapCropBounds(false);
                    text2.setText("当前缩放倍数: x" + df.format(uCropView.getCropImageView().getCurrentScale()));
                }
            });
            //缩放Slider
            Slider slider_Scale = (Slider) findComponentById(ResourceTable.Id_scale);
            slider_Scale.setValueChangedListener(new Slider.ValueChangedListener() {
                @Override
                public void onProgressUpdated(Slider slider, int i, boolean b) {
                    uCropView.getCropImageView().postScale((float) Math.exp(0.001f * (i-50f)), uCropView.getOverlayView().getCropViewRect().getCenter().getPointX(), uCropView.getOverlayView().getCropViewRect().getCenter().getPointY());
                    text2.setText("当前缩放倍数: x" + df.format(uCropView.getCropImageView().getCurrentScale()));
                }
                @Override
                public void onTouchStart(Slider slider) { }
                @Override
                public void onTouchEnd(Slider slider) {
                    uCropView.getCropImageView().setImageToWrapCropBounds(false);
                    slider.setProgressValue(50);
                    text2.setText("当前缩放倍数: x" + df.format(uCropView.getCropImageView().getCurrentScale()));
                }
            });
        }
    }


    private Uri saveImage(String fileName, PixelMap pixelMap) {
        try {
            ValuesBucket valuesBucket = new ValuesBucket();
            valuesBucket.putString(AVStorage.Images.Media.DISPLAY_NAME, fileName);
            valuesBucket.putString("relative_path", "DCIM/");
            valuesBucket.putString(AVStorage.Images.Media.MIME_TYPE, "image/JPEG");
            //应用独占
            valuesBucket.putInteger("is_pending", 1);
            DataAbilityHelper helper = DataAbilityHelper.creator(this);
            int id = helper.insert(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, valuesBucket);
            Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(id));
            //这里需要"w"写权限
            FileDescriptor fd = helper.openFile(uri, "w");
            ImagePacker imagePacker = ImagePacker.create();
            ImagePacker.PackingOptions packingOptions = new ImagePacker.PackingOptions();
            OutputStream outputStream = new FileOutputStream(fd);

            packingOptions.format = "image/jpeg";
            packingOptions.quality = 90;
            boolean result = imagePacker.initializePacking(outputStream, packingOptions);
            if (result) {
                result = imagePacker.addImage(pixelMap);
                if (result) {
                    long dataSize = imagePacker.finalizePacking();
                }
            }
            outputStream.flush();
            outputStream.close();
            valuesBucket.clear();
            //解除独占
            valuesBucket.putInteger("is_pending", 0);
            helper.update(uri, valuesBucket, null);
            return uri;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    public void setFlag(Boolean b){
        MineSlice.flag = b;
    }
    public void setvi(Boolean b){
        MineSlice.vis = b;
    }
}
