package com.huawei.mytestproject.slice;

import com.huawei.mytestproject.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.data.resultset.ResultSet;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;

public class MineSlice extends AbilitySlice {
    public static boolean flag = false,vis=false;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_mine);
        Image image = (Image)findComponentById(ResourceTable.Id_head_portrait);
        if(intent.getFlags() == 0 && flag == false)image.setPixelMap(ResourceTable.Media_num10);
        else{
            if(null != loadImage())image.setPixelMap(loadImage());
            else image.setPixelMap(ResourceTable.Media_num10); }
        if(vis){
            image.setPixelMap(ResourceTable.Media_shuaxin);
            image.setScale((float)0.5,(float)0.5);
            image.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    Intent secondIntent = new Intent();
                    secondIntent.setOperation(new Intent.OperationBuilder()
                            .withBundleName("com.huawei.mytestproject")
                            .withAbilityName("com.huawei.mytestproject.MineAbility")
                            .build());
                    startAbility(secondIntent);
                    vis=false;
                }
            });
        }else {
            image.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    present(new SelectSlice(), new Intent());
                    vis = false;
                }
            });
        }
        //返回按钮
        findComponentById(ResourceTable.Id_tab_back).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if(flag==true){
                    Intent secondIntent = new Intent();
                    secondIntent.setOperation(new Intent.OperationBuilder()
                            .withBundleName("com.huawei.mytestproject")
                            .withAbilityName("com.huawei.mytestproject.MainAbility")
                            .build());
                    startAbility(secondIntent);
                }
                else MineSlice.super.terminate();
                vis=false;
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
    private PixelMap loadImage() {
        DataAbilityHelper helper = DataAbilityHelper.creator(this);
        try {
            ResultSet resultSet = helper.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, new String[]{AVStorage.Images.Media.ID}, null);
            resultSet.goToLastRow();
            ImageSource imageSource = null;
            int id = resultSet.getInt(resultSet.getColumnIndexForName(AVStorage.Images.Media.ID));
            Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(id));
            FileDescriptor fd = helper.openFile(uri, "r");
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            try {
                imageSource = ImageSource.create(fd, null);
                PixelMap pixelMap = imageSource.createPixelmap(decodingOptions);
                return pixelMap;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (imageSource != null) {
                    imageSource.release();
                }
            }
        } catch (DataAbilityRemoteException | FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
