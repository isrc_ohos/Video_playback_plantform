package com.huawei.mytestproject.slice;
import com.huawei.mytestproject.ResourceTable;
import com.youth.banner.Banner;
import com.youth.banner.listener.OnBannerListener;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice implements OnBannerListener {

//    public static boolean flagp=true;
//    public static boolean flagp(){return flagp;}
//    public static boolean setflagp(boolean b){return b;}
     @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        //banner
        Banner banner = (Banner) findComponentById(ResourceTable.Id_banner);
        banner.setOnBannerListener(this);
        List<Integer> list=new ArrayList<Integer>(){{
            add(ResourceTable.Media_w);
            add(ResourceTable.Media_q);
            add(ResourceTable.Media_e);
            add(ResourceTable.Media_r);
            add(ResourceTable.Media_t);

        }};

        List<String> title=new ArrayList<String>(){{

            add("开源软件供应链重大基础设施启动仪式");
            add("数字中国建设峰会");
            add("开源软件供应链2020峰会成功举行");
            add("OpenHarmony2.0共建邀请会");
            add("中科院软件所欢迎你");
        }};

        try {
            banner.setImages(list).setBannerTitles(title).setScaleType(3).setDelayTime(3000).setBannerStyle(5).setTitleTextSize(60).start();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        //扫描按钮
        findComponentById(ResourceTable.Id_tab_zbar).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent secondIntent = new Intent();
                secondIntent.setOperation(new Intent.OperationBuilder()
                        .withBundleName("com.huawei.mytestproject")
                        .withAbilityName("com.huawei.mytestproject.ZBarAbility")
                        .build());
                startAbility(secondIntent);
            }
        });
        //统计按钮
        findComponentById(ResourceTable.Id_tab_charting).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new ChartSlice(),new Intent());
            }
        });
        //我的按钮
        findComponentById(ResourceTable.Id_tab_mine).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new MineSlice(),new Intent());
            }
        });

        //scrollview播放视频
        findComponentById(ResourceTable.Id_layout1).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component Component) {
                Intent secondIntent = new Intent();
                secondIntent.setOperation(new Intent.OperationBuilder()
                        .withBundleName("com.huawei.mytestproject")
                        .withAbilityName("com.huawei.mytestproject.PlayingAbility")
                        .build());
                startAbility(secondIntent);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


    @Override
    public void OnBannerClick(int position) {
        if (position == 0 || position == 1 || position == 2 || position == 3 || position == 4){
           present(new SaleSlice(),new Intent());
        }
    }
}
