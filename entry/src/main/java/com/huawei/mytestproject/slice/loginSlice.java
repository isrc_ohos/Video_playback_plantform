package com.huawei.mytestproject.slice;
import com.huawei.mytestproject.ResourceTable;

import com.huawei.puzzle.puzzleLayout;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;


public class loginSlice extends AbilitySlice {
    //private int o=0;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_login);
        //滑动验证
        DirectionalLayout directionalLayout=(DirectionalLayout) findComponentById(ResourceTable.Id_puzzle);
        directionalLayout.addComponent(new puzzleLayout(this));
//        //登录按键
        Button login_button=(Button) findComponentById(ResourceTable.Id_login_button);
//        //获取用户名
//        TextField username = (TextField)findComponentById(ResourceTable.Id_name_textField);
//        //获取密码
//        TextField password = (TextField)findComponentById(ResourceTable.Id_password_text_field);
        login_button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
//                int i=0;
//                String err="用户名/密码错误";
//                if(username.getText().equals("1")){i++;}
//                if(password.getText().equals("1")){i++;}
//                if(o==0)err="未进行滑动验证";
                //设置登陆验证  i==2  o==1
                //if(i==2&&o==1){present(new MainAbilitySlice(),new Intent());}
                //else {text2.setText(err);}
                present(new MainAbilitySlice(),new Intent());
            }
        });
    }
    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
