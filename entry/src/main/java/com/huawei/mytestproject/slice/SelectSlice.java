package com.huawei.mytestproject.slice;


import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.data.resultset.ResultSet;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;


public class SelectSlice extends AbilitySlice {
    TableLayout tableLayout = new TableLayout(this);
    TableLayout.LayoutConfig layoutConfig = new TableLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
    ScrollView scrollView = new ScrollView(this);
    DirectionalLayout directionalLayout = new DirectionalLayout(this);
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        tableLayout.setColumnCount(4);
        tableLayout.setLayoutConfig(layoutConfig);
        tableLayout.setOrientation(Component.HORIZONTAL);
        showImage();
        directionalLayout.addComponent(tableLayout);
        scrollView.setLayoutConfig(layoutConfig);
        scrollView.addComponent(directionalLayout);
        setUIContent(scrollView);
    }

    private void showImage() {
        DataAbilityHelper helper = DataAbilityHelper.creator(this);
        try {
            // columns为null，查询记录所有字段，当前例子表示查询id字段
            ResultSet resultSet = helper.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, new String[]{AVStorage.Images.Media.ID}, null);
            while (resultSet != null && resultSet.goToNextRow()) {
                PixelMap pixelMap = null;
                ImageSource imageSource = null;
                Image image = new Image(this);
                image.setWidth(250);
                image.setHeight(250);
                image.setMarginsLeftAndRight(10, 10);
                image.setMarginsTopAndBottom(10, 10);
                image.setScaleMode(Image.ScaleMode.CLIP_CENTER);
                // 获取id字段的值
                int id = resultSet.getInt(resultSet.getColumnIndexForName(AVStorage.Images.Media.ID));
                Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(id));
                FileDescriptor fd = helper.openFile(uri, "r");
                ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
                try {
                    imageSource = ImageSource.create(fd, null);
                    pixelMap = imageSource.createPixelmap(null);
                    int height = pixelMap.getImageInfo().size.height;
                    int width = pixelMap.getImageInfo().size.width;
                    float sampleFactor = Math.max(height /250f, width/250f);
                    decodingOptions.desiredSize = new Size((int) (width/sampleFactor), (int)(height/sampleFactor));
                    pixelMap = imageSource.createPixelmap(decodingOptions);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (imageSource != null) {
                        imageSource.release();
                    }
                }
                image.setPixelMap(pixelMap);
                image.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        Intent intent = new Intent();
                        intent.setUri(uri);
                        present(new CropPictureSlice(),intent);
                    }
                });
                tableLayout.addComponent(image);
            }
        } catch (DataAbilityRemoteException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


}
