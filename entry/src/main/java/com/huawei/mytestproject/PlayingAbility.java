package com.huawei.mytestproject;

import com.huawei.mytestproject.slice.PlayingSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class PlayingAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(PlayingSlice.class.getName());
    }
}
