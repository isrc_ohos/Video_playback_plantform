package com.huawei.mytestproject;

import com.huawei.mytestproject.slice.ZBarSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ZBarAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ZBarSlice.class.getName());
    }
}
